package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Trade;
import com.example.demo.repository.TradeRepository;

@Service
public class TradeService {
	@Autowired
	private TradeRepository repository;
	
	public List<Trade> getAllTrades(){
		return repository.getAllTrades();
	}
	
	public Trade getTrade(int id) {
		return repository.getTradeById(id);
	}

	public Trade saveTrade(Trade trader) {
		return repository.editTrade(trader);
	}

	public Trade newTrade(Trade trader) {
		return repository.addTrade(trader);
	}

	public int deleteTrade(int id) {
		return repository.deleteTrade(id);
	}
}
