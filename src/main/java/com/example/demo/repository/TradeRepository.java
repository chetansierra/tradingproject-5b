package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Trade;

@Component
public interface TradeRepository {
	public List<Trade> getAllTrades();
	public Trade getTradeById(int id); 
	public Trade editTrade(Trade trade);
	public int deleteTrade(int id);
	public Trade addTrade(Trade trade);
}
